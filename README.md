# <center>Practicals</center>

## <center>Masters of Science(Computer Science)  AY 2021-2022 and 2022-2023</center>

---

### Introduction

This repository houses the practicals executed by the students of course Masters of Science(Computer Science) for the academic years 2021-2022 and 2022-2023.



### Semester 1

| Subject Name   | Folder link    |
|--------------- | --------------- |
| Analysis of Algorithms and Reasearch in Parallel Computing   | [link](./Semester%201/Algorithms/)   |
| Fundamentals of Data Science   | [link](./Semester%201/Fundamentals%20of%20Data%20Science/)   |
| Bioinformatics   | [link](./Semester%201/Bioinformatics/)   |
| Data Warehousing and Data Mining   | [link](./Semester%201/Data%20Warehousing%20and%20Data%20Mining/)   |


### Semester 2

| Subject Name   | Folder link    |
|--------------- | --------------- |
| Design and Implementation of Modern Compilers   | [link](./Semester%202/Design%20and%20implementation%20of%20Modern%20Compilers/)   |
| Advanced Embedded Systems   | [link](./Semester%202/Advanced%20Embedded%20Systems/)   |
| Social Network Analysis   | [link](./Semester%202/Social%20Network%20Analysis/)   |
| Business Intelligence and Big Data Analytics   | [link](./Semester%202/Business%20Intelligence%20and%20Big%20Data%20Analytics/)   |


### Semester 3

| Subject Name   | Folder link    |
|--------------- | --------------- |
| Machine and Deep Learning   | [link](./Semester%203/Machine%20Learning/)   |
| Cyber Forensics and Laws   | [link](./Semester%203/Cyber%20Forensics%20and%20Laws/)   |
| Operation Research   | [link](./Semester%203/Operations%20Research/)   |
| Robotics   | [link](./Semester%203/Robotics/)   |