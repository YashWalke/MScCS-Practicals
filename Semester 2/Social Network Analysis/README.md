# <center>Social Network Analysis</center>

## <center>Masters of Science(Computer Science) Part I AY 2021-2022 Semester 2</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part I for the subject **"Social Network Analysis"** (Semester 2).



### Practical List
<table>
    <tr>
        <th>Practical No.</th>
        <th>Practical Code (as a single R file)</th>
        <th>Output(s)</th>
    <tr>
    <tr>
        <td>1</td>
        <td><a href = "src/Practical 1.R">Practical 1</a></td>
        <td><a href = "output/screenshots/Practical 1">screenshots</a></td>
    </tr>
    <tr>
        <td>2</td>
        <td><a href = "src/Practical 2.R">Practical 2</a></td>
        <td><a href = "output/screenshots/Practical 2">screenshots</a> and <a href = "output/plots/Practical 2">plots</a></td>
    </tr>
    <tr>
        <td>3</td>
        <td><a href = "src/Practical 3.R">Practical 3</a></td>
        <td><a href = "output/screenshots/Practical 3">screenshots</a> and <a href = "output/plots/Practical 3">plots</a></td>
    </tr>
    <tr>
        <td>4</td>
        <td><a href = "src/Practical 4.R">Practical 4</a></td>
        <td><a href = "output/screenshots/Practical 4">screenshots</a> and <a href = "output/plots/Practical 4">plots</a></td>
    </tr>
    <tr>
        <td>5</td>
        <td><a href = "src/Practical 5.R">Practical 5</a></td>
        <td><a href = "output/screenshots/Practical 5">screenshots</a> and <a href = "output/plots/Practical 5">plots</a></td>
    </tr>
    <tr>
        <td>6</td>
        <td><a href = "src/Practical 6.R">Practical 6</a></td>
        <td><a href = "output/screenshots/Practical 6">screenshots</a> and <a href = "output/plots/Practical 6">plots</a></td>
    </tr>
    <tr>
        <td>7</td>
        <td><a href = "src/Practical 7.R">Practical 7</a></td>
        <td><a href = "output/screenshots/Practical 7">screenshots</a></td>
    </tr>
</table>



## <center> Mini Project </center>

The mini project can be found in the [mini_project](./mini_project) subdirectory.