# <center>Advanced Embedded Systems </center>

## <center>Masters of Science(Computer Science) Part I AY 2021-2022 Semester 2</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part I for the subject **"Advanced Embedded Systems"** (Semester 2).

### Practical List
<table>
    <tr>
        <th>Practical No.</th>
        <th>Arduino Source Code (.ino) file</th>
    </tr>
    <tr>
        <td>1</td>
        <td><a href = "src/Practical 1.ino">Practical 1</a></td>
    </tr>
    <tr>
        <td>2</td>
        <td><a href = "src/Practical 2.ino">Practical 2</a></td>
    </tr>
    <tr>
        <td>3</td>
        <td><a href = "src/Practical 3.ino">Practical 3</a></td>
    </tr>
    <tr>
        <td>4</td>
        <td><a href = "src/Practical 4.ino">Practical 4</a></td>
    </tr>
    <tr>
        <td>5</td>
        <td><a href = "src/Practical 5.ino">Practical 5</a></td>
    </tr>
    <tr>
        <td>6</td>
        <td><a href = "src/Practical 6a.ino">Practical 6a</a> and <a href = "src/Practical 6b.ino">Practical 6b</a></td>
    </tr>
    <tr>
        <td>7</td>
        <td><a href = "src/Practical 7.ino">Practical 7</a></td>
    </tr>
    <tr>
        <td>8</td>
        <td><a href = "src/Practical 8.ino">Practical 8</a></td>
    </tr>
    <tr>
        <td>9</td>
        <td><a href = "src/Practical 9.ino">Practical 9</a></td>
    </tr>
    <tr>
        <td>10</td>
        <td><a href = "src/Practical 10.ino">Practical 10</a></td>
    </tr>
</table>



## <center> Mini Project </center>

The mini project can be found in the [mini_project](./mini_project) subdirectory.