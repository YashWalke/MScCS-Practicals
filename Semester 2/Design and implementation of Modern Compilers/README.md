# <center>Design and Implementation of Modern Compilers </center>

## <center>Masters of Science(Computer Science) Part I AY 2021-2022 Semester 2</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part I for the subject **"Design and Implementation of Modern Compilers"** (Semester 2).


### Practical List
<table>
    <tr>
        <th>Practical No.</th>
        <th>Practical Code</th>
        <th>Output(s)</th>
    </tr>
    <tr>
        <td>1</td>
        <td><a href = "src/ndfa.py">ndfa.py</a></td>
        <td><a href = "output/screenshots/Practical 1">screenshots</a> and <a href = "output/text_files/Practical 1">Text files</a></td>
    </tr>
    <tr>
        <td>2</td>
        <td><a href = "src/llg_to_rlg.py">llg_to_rlg.py</a></td>
        <td><a href = "output/screenshots/Practical 2">screenshots</a> and <a href = "output/text_files/Practical 2">Text files</a></td>
    </tr>
    <tr>
        <td>3</td>
        <td><a href = "src/dag.py">dag.py</a></td>
        <td><a href = "output/screenshots/Practical 3">screenshots</a> and <a href = "output/text_files/Practical 3">Text files</a></td>
    </tr>
    <tr>
        <td>4</td>
        <td><a href = "src/triples.py">triples.py</a></td>
        <td><a href = "output/screenshots/Practical 4">screenshots</a> and <a href = "output/text_files/Practical 4">Text files</a></td>
    </tr>
    <tr>
        <td>5</td>
        <td><a href = "src/postfix.py">postfix.py</a></td>
        <td><a href = "output/screenshots/Practical 5">screenshots</a> and <a href = "output/text_files/Practical 5">Text files</a></td>
    </tr>
    <tr>
        <td>6</td>
        <td><a href = "src/3_address.py">3_address.py</a></td>
        <td><a href = "output/screenshots/Practical 6">screenshots</a> and <a href = "output/text_files/Practical 6">Text files</a></td>
    </tr>
    <tr>
        <td>7</td>
        <td><a href = "src/loop_jamming.py">loop_jamming.py</a></td>
        <td><a href = "output/screenshots/Practical 7">screenshots</a> and <a href = "output/text_files/Practical 7">Text files</a></td>
    </tr>
    <tr>
        <td>8</td>
        <td><a href = "src/loop_unrolling.py">loop_unrolling.py</a></td>
        <td><a href = "output/screenshots/Practical 8">screenshots</a> and <a href = "output/text_files/Practical 8">Text files</a></td>
    </tr>
</table>



## <center> Mini Project </center>

The mini project can be found in the [mini_project](./mini_project) subdirectory.