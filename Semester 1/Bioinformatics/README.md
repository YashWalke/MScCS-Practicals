# <center>Bio-informatics</center>

## <center>Masters of Science(Computer Science) Part I AY 2021-2022 Semester 1</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part I for the subject "Bio-informatics" (Semester 1).


### Practical list
<table>
	<tr>
		<th>Practical No. </th>
		<th>Associated Python code </th>
	</tr>
	<tr>
		<td> Practical 1 </td>
		<td> <a href = "pairwise_alignment.py">pairwise_alignment.py</a></td>
	</tr>
	<tr>
		<td> Practical 2 </td>
        <td><a href = "identity.py">identity.py</a></td>
	</tr>
	<tr>
		<td> Practical 3 </td>
		<td><a href = "similarity.py">similarity.py</a></td>
	</tr>
	<tr>
		<td> Practical 4 </td>
		<td><a href = "percent_matching.py">percent_matching.py</a></td>
	</tr>
	<tr>
		<td> Practical 5 </td>
		<td><a href = "global_alignment.py">global_alignment.py</a></td>
	</tr>
	<tr>
		<td> Practical 6 </td>
		<td><a href = "multiple_sequence_alignment.py">multiple_sequence_alignment.py</a></td>
	</tr>
	<tr>
		<td> Practical 7 </td>
		<td><a href = "regular_expression.py">regular_expression.py</a></td>
	</tr>
	<tr>
		<td> Practical 8 </td>
		<td><a href = "fingerprint.py">fingerprint.py</a></td>
	</tr>
	<tr>
		<td> Practical 9 </td>
		<td><a href = "Motif.py">Motif.py</a></td>
	</tr>
	<tr>
		<td> Practical 10 </td>
		<td><a href = "BLAST.py">BLAST.py</a></td>
	</tr>
</table>


### Used genomes

<table>
    <tr>
    	<th>Scientific name</th>
        <th>File name</th>
        <th>Wikipedia page</th>
    </tr>
    <tr>
    	<td>Pithovirus Sibericum</td>
        <td><a href = "used_genomes/Pithovirus_Sibericum.txt">Pithovirus_Sibericum.txt</a></td>
        <td><a href = "https://en.wikipedia.org/wiki/Pithovirus">Pithovirus</a></td>
    </tr>
    <tr>
    	<td>Salmonella Enterica</td>
        <td><a href = "used_genomes/Salmonella_Enterica.txt">Salmonella_Enterica.txt</a></td>
        <td><a href = "https://en.wikipedia.org/wiki/Salmonella_enterica">Salmonella Enterica</a></td>
    </tr>
    <tr>
    	<td>Variola</td>
        <td><a href = "used_genomes/Variola.txt">Variola.txt</a></td>
        <td><a href = "https://en.wikipedia.org/wiki/Smallpox">Smallpox</a></td>
    </tr>
</table>


### Mini project

The mini project can be found in the [mini_project](./mini_project) subdirectory.
