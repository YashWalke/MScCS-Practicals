# <center>Fundamentals of Data Science Practicals </center>

## <center>Masters of Science(Computer Science) Part I AY 2021-2022 Semester 1</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part I for the subject "Fundamentals of Data Science" (Semester 1).

### Practical list

<table>
	<tr>
		<th>Practical No. </th>
		<th>Associated Python code (IPYNB format) </th>
        <th>Associated Python code (PDF format)</th>
	</tr>
	<tr>
		<td> Practical 1 </td>
		<td> <a href = "Practical 1.ipynb">Practical 1.ipynb</a></td>
        <td> <a href = "practicals_pdf/Practical 1.pdf">Practical 1.pdf</a></td>
	</tr>
	<tr>
		<td> Practical 2 </td>
        <td><a href = "Practical 2.ipynb">Practical 2.ipynb</a></td>
        <td> <a href = "practicals_pdf/Practical 2.pdf">Practical 2.pdf</a></td>
	</tr>
	<tr>
		<td> Practical 3 </td>
		<td><a href = "Practical 3.ipynb">Practical 3.ipynb</a></td>
        <td> <a href = "practicals_pdf/Practical 3.pdf">Practical 3.pdf</a></td>
	</tr>
	<tr>
		<td> Practical 4 </td>
		<td><a href = "Practical 4.ipynb">Practical 4.ipynb</a></td>
        <td> <a href = "practicals_pdf/Practical 4.pdf">Practical 4.pdf</a></td>
	</tr>
	<tr>
		<td> Practical 5 </td>
		<td><a href = "Practical 5.ipynb">Practical 5.ipynb</a></td>
        <td> <a href = "practicals_pdf/Practical 5.pdf">Practical 5.pdf</a></td>
    </tr>
    <tr>
		<td> Practical 6 </td>
		<td><a href = "Practical 6.ipynb">Practical 6.ipynb</a></td>
        <td> <a href = "practicals_pdf/Practical 6.pdf">Practical 6.pdf</a></td>
    </tr>
</table>

**<u>Note:</u>** 

1. The PDF files are generated using the `Print>Microsoft Print to PDF` command. So it may have some formatting issues as the IPYNB filetype does not support paging.
2. Some practicals generate output files. These can be found in the [output_files](./output_files/) subdirectory.
3. The data sets used in certain practicals can be found in the [data_sets](./data_sets) subdirectory.



### Mini project

The mini project can be found in the [mini_project](./mini_project) subdirectory.
