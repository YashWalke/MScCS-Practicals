# <center>Analysis of Algorithms and Researching Computing </center>

## <center>Masters of Science(Computer Science) Part I AY 2021-2022 Semester 1</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part I for the subject "Analysis of Algorithms and Researching Computing" (Semester 1).

### Practical list

<table>
	<tr>
		<th>Practical No. </th>
		<th>Associated Python code</th>
	</tr>
	<tr>
		<td> Practical 1 </td>
		<td> <a href = "randomized_selection.py">randomized_selection.py</a></td>
	</tr>
	<tr>
		<td> Practical 2 </td>
        <td><a href = "heap_sort.py">heap_sort.py</a></td>
    </tr>
	<tr>
		<td> Practical 3 </td>
		<td><a href = "radix_sort.py">radix_sort.py</a></td>
	</tr>
	<tr>
		<td> Practical 4 </td>
		<td><a href = "bucket_sort">bucket_sort.py</a></td>
	</tr>
	<tr>
		<td> Practical 5 </td>
		<td><a href = "floyd_warshall.py">floyd_warshall.py</a></td>
	</tr>
    <tr>
		<td> Practical 6 </td>
		<td><a href = "counting_sort.py">counting_sort.py</a></td>
	</tr>
    <tr>
		<td> Practical 7 </td>
		<td><a href = "set_cover.py">set_cover.py</a></td>
	</tr>
    <tr>
		<td> Practical 8 </td>
		<td><a href = "subset_sum.py">subset_sum.py</a></td>
	</tr>
</table>
**<u>Note:</u>** Output Screenshots can be found in the [screenshots](./screenshots) subdirectory.



## <center> Mini Project </center>

The mini project can be found in the [mini_project](./mini_project) subdirectory.
