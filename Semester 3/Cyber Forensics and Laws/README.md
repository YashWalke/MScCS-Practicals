# <center>Cyber Forensics and Laws</center>

## <center>Masters of Science(Computer Science) Part II AY 2021-2022 Semester 3</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part II for the subject **"Cyber Forensics and Laws"** (Semester 3).

### Practical List
| Practical No.  | Content   | Output   |
|-------------- | -------------- | -------------- |
| 1    | [link](./src/Practical%201/)     | [link](./screenshots/Practical%201/)     |
| 2    | [link](./src/Practical%202/)     | [link](./screenshots/Practical%202/)     |
| 3    | [link](./src/Practical%203/)     | [link](./screenshots/Practical%203/)     |
| 4    | [link](./src/Practical%204/)     | [link](./screenshots/Practical%204/)     |
| 5    | [link](./src/Practical%205/)     | [link](./screenshots/Practical%205/)     |
| 6    | [link](./src/Practical%206/)     | [link](./screenshots/Practical%206/)     |
| 7    | [link](./src/Practical%207/)     | [link](./screenshots/Practical%207/)     |
| 8    | [link](./src/Practical%208/)     | [link](./screenshots/Practical%208/)     |
| 9    | [link](./src/Practical%209/)     | [link](./screenshots/Practical%209/)     |