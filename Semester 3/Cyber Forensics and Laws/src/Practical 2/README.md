# <center>Practical 2</center>
---
**<u>Aim:</u>** Create a logger in Java.


**File(s):**
+ [CustomLogger.java](./CustomLogger.java)


**Screenshots:**
![log.txt](../../screenshots/Practical%202/log-file.png)