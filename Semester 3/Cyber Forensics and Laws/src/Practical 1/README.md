# <center>Practical 1</center>
---
**<u>Aim:</u>** Create a Java project that encrypts a message on the sender end, broadcasts it over a channel to the receiver, which decrypts it.


**Project files:**
+ [Sender.java](./Sender.java)
+ [Receiver.java](./Receiver.java)


**Screenshots:**

![sender.png](../../screenshots/Practical%201/sender.png)
<center>Sender</center>


![receiver.png](../../screenshots/Practical%201/receiver.png)
<center>Receiver</center>