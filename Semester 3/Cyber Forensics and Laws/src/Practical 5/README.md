# <center>Practical 5</center>
---
**<u>Aim:</u>** Create a Java program to create a virus that eats up space on the disk.


**<u>File(s):</u>**
+ [VirusExample.java](./VirusExample.java)


**<u>Screenshots:</u>**
![Before](../../screenshots/Practical%205/screenshot-1.png)

<center>Before</center>


![Created File](../../screenshots/Practical%205/screenshot-3.png)

<center>Created File</center>


![After](../../screenshots/Practical%205/screenshot-2.png)

<center>After</center>