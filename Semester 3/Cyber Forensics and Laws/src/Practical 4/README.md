# <center>Practical 4</center>
---
**<u>Aim:</u>** Create a Java program to search a particular word in a file.


**<u>File(s):</u>**
+ [FileSearcher.java](./FileSearcher.java)


**<u>Screenshots:</u>**
![screenshot-1.png](../../screenshots/Practical%204/screenshot-1.png)