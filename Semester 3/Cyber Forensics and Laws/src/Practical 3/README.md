# <center>Practical 3</center>
---
**<u>Aim:</u>** Create a Java program to search for files in a given directory.


**<u>File(s):</u>**
+ [DirectorySearcher.java](./DirectorySearcher.java)


**<u>Screenshots:</u>**
+ ![screenshot-1.png](../../screenshots/Practical%203/screenshot-1.png)