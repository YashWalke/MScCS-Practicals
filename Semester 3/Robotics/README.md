# <center>Robotics</center>

## <center>Masters of Science(Computer Science) Part II AY 2021-2022 Semester 3</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part II for the subject **"Robotics"** (Semester 3).

### Practical List

| Practical No.  | Source code   |
|-------------- | -------------- |
| 1    | [BasicRobot.java](./src/BasicRobot.java) & [GearRobot.java](./src/GearRobot.java)     |
| 2    | [MotorRobot.java](./src/MotorRobot.java)     |
| 3    | [RobotSquare.java](./src/RobotSquare.java)     |
| 4    | [LineFollowerRobot.java](./src/LineFollowerRobot.java)     |
| 5    | [MotorRobotCircle.java](./src/MotorRobotCircle.java)     |
| 6    | [PathFollowerRobot.java](./src/PathFollowerRobot.java)     |
| 7    | [ObstacleFindingRobot.java](./src/ObstacleFindingRobot.java)     |
| 8    | [UltrasonicObstacleFinder.java](./src/UltrasonicObstacleFinder.java)     |



**<u>Note</u>**:
+ For practicals based in Java, use the [`JGameGrid.jar`](./libs/JGameGrid.jar) and [`robotsim.jar`](./libs/RobotSim.jar) <abbr title = "Java Archive">JAR</abbr> files.

+ Add [this](./sprites/) directory to the root folder of your project.

+ Custom backgrounds can be found in the above mentioned folder.