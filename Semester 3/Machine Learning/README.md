# <center>Machine Learning</center>

## <center>Masters of Science(Computer Science) Part II AY 2021-2022 Semester 3</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part II for the subject **"Machine Learning"** (Semester 3).

### Practical List
| Practical No.   | Source Code    |
|--------------- | --------------- |
| 1   | [Simple Linear Regression](./src/SLR.ipynb)   |
| 2   | [Multiple Linear Regression](./src/MLR.ipynb)   |
| 3   | [Support Vector Machine](./src/SVM.ipynb)   |
| 4   | [k Nearest Neighbours](./src/KNN.ipynb)   |
| 5   | [Hierarchical Clustering](./src/HClust.ipynb)   |
| 6   | [k Means clustering](./src/KMeans.ipynb)   |
| 7   | [Artificial Neural Network](./src/ANN.ipynb)   |
| 8   | [Convolutionary Neural Network](./src/CNN.ipynb)   |