# <center>Operations Research</center>

## <center>Masters of Science(Computer Science) Part II AY 2021-2022 Semester 3</center>

---

### Introduction

This repository houses the practicals executed by the students of batch 2021-2022 of the course Masters of Science(Computer Science) Part II for the subject **"Operations Research"** (Semester 3).


### Practical list
| Practical No.    | Content    | Output    |
|---------------- | --------------- | --------------- |
| 1    | [Graphical Method](./src/R/GraphicalMethod.R)    | [screenshots](./screenshots/Practical%201/)  [plot](./plots/Practical%201/plot-1.png)    |
| 2    | [Simplex Method for 2 variables](./src/Python/SimplexFor2Vars.ipynb)   | N/A    |
| 3    | [Simplex Method for 3 variables](./src/Python/SimplexFor3Vars.ipynb)   | N/A    |
| 4    | [Simplex Method for an equality constraint](./src/Python/SimplexForEquality.ipynb)   | N/A    |
| 5    | [Big M method](./src/Python/BigM.ipynb)   | N/A    |
| 6    | [Resource Allocation Problem](./src/Python/ResourceAllocation.ipynb)   | N/A    |
| 7    | [Infeasibilty](./src/Python/Infeasibility.ipynb)   | N/A    |
| 8    | [Dual Simplex Method](./src/R/DualSimplex.R)   | [screenshots](./screenshots/Practical%208/)    |
| 9    | [Transportation Problem](./src/R/Transportation.R)   | [screenshots](./screenshots/Practical%209/)    |
| 10   | [Assignment Problem](./src/R/Assignment.R)   | [screenshots](./screenshots/Practical%2010/)    |